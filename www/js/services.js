angular.module('starter.services', [])
.factory('newsFactory', ['$http', function($http) {
    var urlBase = 'http://www.mehrnews.com/api';
    var news = {};

    news.get = function (type) {
        if (type=='latest')
            var _url = urlBase;
        else if (type=='primary')
            var _url = urlBase + '?place=1,2,3&size=5';
            else if (parseInt(type)!=NaN)
                var _url = urlBase+ '?topic=' + type;
        return $http.get(_url);
    };
    news.preparedNews = function(){
      var storedNews = localStorage.getItem('newsList');
      _news = JSON.parse(storedNews);
      return _news;

  }
  news.getAllPrimeries = function(){
      // http://www.mehrnews.com/api?place=13
      _url = urlBase + '?place=13&size=50';
      return $http.get(_url);
      }
  
  news.getPrimaryNews = function(){

      // http://mehrnews.com/api?place=1,2,3&size=5
      return $http.get(primaryNewsUrl);
  }
  news.getItem = function(itemId){
      var itemUrl = urlBase + '?news=' + itemId;
      return $http.get(itemUrl);
  }
  news.cache = [];

  news.getAllPrimeries();
  return news;
}])

.factory('prettyDate', [function($http) {

 return function (time) {
  moment.locale('fa');
  var _d = moment(time);
  return moment.duration(_d - moment()).humanize();
};
}]);
