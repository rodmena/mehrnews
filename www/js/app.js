// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var ngapp = angular.module('starter', ['ionic', 'ngCordova', 'starter.controllers', 'starter.services'])

ngapp.run(function($ionicPlatform,$rootScope, $ionicLoading,
            $cordovaSplashscreen, $timeout) {
        /*
                $timeout(function() {
             $cordovaSplashscreen.hide()
         }, 2000)
         */
  $ionicPlatform.ready(function() {
/*
$rootScope.$on('loading:show', function() {
   $ionicLoading.show({template: 'Loading News'})
 })

 $rootScope.$on('loading:hide', function() {
   $ionicLoading.hide()
 })
 */
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
})

ngapp.config(function($stateProvider, $urlRouterProvider, $httpProvider) {
/*
    $httpProvider.interceptors.push(function($rootScope) {
      return {
        request: function(config) {
          $rootScope.$broadcast('loading:show')
          return config
        },
        response: function(response) {
          $rootScope.$broadcast('loading:hide')
          return response
        }
      }
  })
  */
  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html',
    controller: 'TabsCtrl'

  })

  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('tab.timeline', {
      url: '/timeline',
      views: {
        'tab-timeline': {
          templateUrl: 'templates/tab-timeline.html',
          controller: 'TimelineCtrl',
        }
      }
    })
    .state('tab.topic', {
        url: '/timeline/:topicId',
        views: {
          'tab-timeline': {
            templateUrl: 'templates/tab-timeline.html',
            controller: 'TimelineCtrl',
          }
        }
      })

    .state('tab.item', {
      url: '/item/:itemId',
      views: {
        'tab-timeline': {
          templateUrl: 'templates/news-item.html',
          controller: 'NewsItemCtrl'
        }
      }
    })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/dash');

});

ngapp.config(['$ionicConfigProvider', function($ionicConfigProvider) {

    $ionicConfigProvider.tabs.position('bottom'); // other values: top

}]);
