

sections = {
    'هنر': 1,
    'فرهنگ':2,
    'دین و اندیشه':3,
    'حوزه و دانشگاه':5,
    'دانش و فناوری':6,
    'جامعه':7,
    'سیاست':8,
    'ورزش':9,
    'بین الملل':10,
    'عکس':11,

}

function checkRtl( character ) {
    var RTL = ['ا','ب','پ','ت','س','ج','چ','ح','خ','د','ذ','ر','ز','ژ','س','ش','ص','ض','ط','ظ','ع','غ','ف','ق','ک','گ','ل','م','ن','و','ه','ی'];
    return RTL.indexOf( character ) > -1;
};


function findById(source, id) {
    if (!source)
        return;
  for (var i = 0; i < source.length; i++) {
    if (source[i].id == id) {
      return source[i];
    }
  }
  throw "Couldn't find object with id: " + id;
}

function findImaged(source) {
    if (!source)
        return;
  var result = [];
  for (var i = 0; i < source.length; i++) {
    if (source[i].largeThumbnail) {
      result.push(source[i]);
    }

  }
  return result;
 }

 function findTopicTitle(source, id) {
     if (!source)
         return;
   for (var i = 0; i < source.length; i++) {
     if (source[i].topicId == id) {
       return source[i];
     }
   }
   throw "Couldn't find object with id: " + id;
 }


function findBoxNews(source) {
  var result = [];
  for (var i = 0; i < source.length; i++) {
      place = source[i];
      for (var k = 0; k < place.newsList.length; k++) {
          result.push(place.newsList[k])
      }

  }
  return result;
 }


var isASCII = function (str, extended) {
  var data = (extended ? /^[\x00-\xFF]*$/ : /^[\x00-\x7F]*$/).test(str);
  return data;
};




angular.module('starter.controllers', ['ionic', 'ngSanitize'])

.controller('AppCtrl', function($scope, $ionicModal, $ionicPopover,
    $rootScope, $timeout, newsFactory) {
        $rootScope.toggleLeft = function() {
          $ionicSideMenuDelegate.toggleLeft();
        }

    // Form data for the login modal
    //console.log(newsFactory.getAllPrimeries);
    $scope.getAllPrimeries = function(){
    newsFactory.getAllPrimeries().success(function(data){
        $rootScope.allPrimeries = data.newsList;
    })
    }
    $scope.getAllPrimeries();
})


.controller('DashCtrl', function($scope, $ionicModal, $rootScope, $interval,
            $location, newsFactory, $timeout, prettyDate, $ionicLoading,
            $ionicSideMenuDelegate, $http) {



  $scope.prepareCover = function(){
      ionic.DomUtil.ready(function(){
            var canvas = document.getElementById('coverCanvas');
            var stage = new createjs.Stage(canvas);
            console.log(stage.width);

            var image = new Image();
              image.src = $scope.coverImage;
              image.onload = handleImageLoad;

          function handleImageLoad(event) {

              var image = event.target;
              width = image.width;
              height = image.height;
              canvas.width=width;
              canvas.height=height;
              var bitmap = new createjs.Bitmap(image, "cover");
              //var backg = new createjs.Rectangle(0, 0, 100, 100);
              var text = new createjs.Text($scope.cover.headline, "bold 48px Terafik", "#FFF");
              text.textAlign = 'center';
              text.lineWidth = width-(width/10);
              text.x = width/2;
              text.y = height-128;

              stage.addChild(bitmap, text);
              //var circle = new createjs.Shape();
              //circle.graphics.beginFill("DeepSkyBlue").drawCircle(0, 0, 50);
              //circle.x = 100;
              //circle.y = 100;
              //stage.addChild(circle);
              stage.update();
          }
  })
  };

  $scope.prepareContents = function(){
      $scope.coverImage = $scope.imagedNews[0].largeThumbnail;
      $scope.cover = $scope.imagedNews[0];

  }

  $scope.prettyDate = prettyDate;

  $scope.isFetching = false;
  $scope.news = {};
  $scope.fetchPrimaryNews = function(){
      $ionicLoading.show({
        templateUrl: 'templates/loading.html'
      })
      $scope.isFetching = true;
      newsFactory.get('primary')
      .success(function(result){
            _news = result.placeNewsList;
            $scope.news = findBoxNews(_news);
            $scope.imagedNews = findImaged($scope.news);

      })
      .finally(function(result){
          $scope.prepareContents();
          //$scope.buildCover();
          $scope.$broadcast('scroll.refreshComplete');
          $scope.isFetching = false;
          newsFactory.cache = $scope.news;
          $ionicLoading.hide();



      })

    };
  $scope.fetchPrimaryNews();





    $scope.navTitle='<img class="title-image" src="img/logo.png" />';
    $ionicModal.fromTemplateUrl('templates/loading-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
      //$scope.modal.initialize();
    });
    $scope.openModal = function() {
      $scope.modal.show();
    };
    $scope.closeModal = function() {
      $scope.modal.hide();
    };
    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hidden', function() {
      // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function() {
      // Execute action
    });


  })


.controller('TimelineCtrl', function($scope, $rootScope, $interval, $timeout,
    newsFactory, prettyDate, $location,  $stateParams
            , $ionicLoading) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  //window.location ='/#/tab/timeline';

  $scope.topicId = $stateParams.topicId;
  if (!$scope.topicId)
        $scope.topicId = 'latest';
  if ($scope.topicId!='latest')
        $scope.cover = findTopicTitle($rootScope.allPrimeries, $scope.topicId);
  $scope.fetchLatestNews = function(section){
      $ionicLoading.show({
        templateUrl: 'templates/loading.html'
        //template: 'در حال دریافت آخرین اخبار'
      })
      $scope.isFetching = true;
      newsFactory.get($scope.topicId)
      .success(function(result){
            $scope.news = result.newsList;
      })
      .finally(function(data){

          $ionicLoading.hide();
          $scope.$broadcast('scroll.refreshComplete');
         // Delay expansion


      })
  };
  $scope.prettyDate = prettyDate;
  $scope.fetchLatestNews();



 // Set Motion

 // Set Ink
})

.controller('NewsItemCtrl', function($scope, $rootScope, $stateParams,
    newsFactory, prettyDate, $timeout, $cordovaFileTransfer, $ionicLoading) {
  $scope.prettyDate = prettyDate;
      $scope.init = function(){
          $ionicLoading.show({
            templateUrl: 'templates/loading.html'
          })

          itemPromise = newsFactory.getItem($stateParams.itemId);
          itemPromise.success(function(result){
              $scope.item = result.newsList[0];
              imgurl = $scope.item.largeThumbnail;





          }).finally(function(_data){
              $ionicLoading.hide();
          // Set Motion
      })

  }
  })


.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableAutoFetch: true
  };
})

.controller('PlatformCtrl', function($scope) {

  ionic.Platform.ready(function(){
      //var fileTransfer = new FileTransfer();
    // will execute when device is ready, or immediately if the device is already ready.
  });

  var deviceInformation = ionic.Platform.device();

  var isWebView = ionic.Platform.isWebView();
  var isIPad = ionic.Platform.isIPad();
  var isIOS = ionic.Platform.isIOS();
  var isAndroid = ionic.Platform.isAndroid();
  var isWindowsPhone = ionic.Platform.isWindowsPhone();

  var currentPlatform = ionic.Platform.platform();
  var currentPlatformVersion = ionic.Platform.version();

  ionic.Platform.exitApp(); // stops the app
})

.controller('RightMenuCtrl', function($scope, $ionicModal, $rootScope, $interval,
            $location, newsFactory, $timeout, prettyDate, $ionicLoading,
            $ionicSideMenuDelegate) {
                $scope.sections = sections;
                $scope.topics = Object.keys(sections);

    })
    .controller('TabsCtrl', ['$scope', '$location', '$state', function($scope, $location, $state) {
        $scope.goToTimeline = function() {
            $state.go("tab.timeline");
        }
        $scope.goToMainPage = function() {
            $state.go("tab.dash");
        }
    }]);
